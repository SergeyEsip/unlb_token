const time = require("./time_travel.js");
const ICO = artifacts.require('./ICO.sol');
const UNLB = artifacts.require('./UNLB.sol');

async function test() {
  const acc = web3.eth.accounts;
  const ico = await ICO.new()
  const unlb = UNLB.at(await ico.unlb.call());

  assert.isOk(ico && ico.address, "ico has invalid address");
  assert.isOk(unlb && unlb.address, "unlb has invalid address");

  const icoStartTime = (await ico.ICO_START_DATE.call()).toFixed();
  const timeAndBonus = [
    {addr: acc[9], time: icoStartTime, price: 650},
    {addr: acc[8], time: 1508342401,   price: 575},
    {addr: acc[7], time: 1509033601,   price: 537.5},
    {addr: acc[6], time: 1509811201,   price: 500}
  ];

  for(const x of timeAndBonus) {
    await time.travelTo(x.time);
    const currentTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp;
    assert.isOk(currentTime >= x.time);

    await web3.eth.sendTransaction(
      { from: x.addr
      , to: ico.address
      , value: 123
      , gas: 123000
      });
    const balance = await unlb.balanceOf.call(x.addr);
    assert.equal(Math.floor(123 * x.price), balance.toFixed(),
      "balance is updated at price " + x.price);
  }

  const subTotal = await unlb.totalSupply.call(); // 278287
  assert.equal(Math.floor(123 * (650 + 575 + 537.5 + 500)) , subTotal.toFixed(),
    "total balance")

  const icoEndTime = (await ico.ICO_END_DATE.call()).toFixed();
  await time.travelTo(icoEndTime);

  await ico.finish(acc[0], acc[1], acc[2], {from: acc[0]});
  assert.isOk(await ico.isFinished.call(), "finished");

  const total = (await unlb.totalSupply.call()).toFixed();
  const team = await unlb.balanceOf.call(acc[0]);
  const fund = await unlb.balanceOf.call(acc[1]);
  const bounty = await unlb.balanceOf.call(acc[2]);
  assert.isOk(2 > Math.abs(total * 0.12 - team.toFixed()), "team balance");
  assert.isOk(2 > Math.abs(total * 0.10 - fund.toFixed()), "fund balance");
  assert.isOk(2 > Math.abs(total * 0.02 - bounty.toFixed()), "bounty balance");
}

test().then(() => console.log("DONE"))
