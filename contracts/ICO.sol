pragma solidity ^0.4.15;

import 'zeppelin-solidity/contracts/lifecycle/Pausable.sol';
import './UNLB.sol';


contract ICO is Pausable {

  uint public constant ICO_START_DATE = /*2017-09-04 12:00:00+8*/ 1504497600;
  uint public constant ICO_END_DATE   = /*2017-11-21 00:00:00+8*/ 1511193600;

  address public constant admin      = 0xFeC0714C2eE71a486B679d4A3539FA875715e7d8;
  address public constant teamWallet = 0xf16d5733A31D54e828460AFbf7D60aA803a61C51;

  UNLB public unlb;
  bool public isFinished = false;

  function ICO() {
    owner = admin;
    unlb = new UNLB();
    unlb.pause();
  }

  function pricePerWei() public constant returns(uint) {
    if     (now < /*2017-10-19 00:00:00+8*/ 1508342400) return 650.0 * 1 ether;
    else if(now < /*2017-10-27 00:00:00+8*/ 1509033600) return 575.0 * 1 ether;
    else if(now < /*2017-11-05 00:00:00+8*/ 1509811200) return 537.5 * 1 ether;
    else                                                return 500.0 * 1 ether;
  }


  function() public payable {
    require(!paused && now >= ICO_START_DATE && now < ICO_END_DATE);
    uint _tokenVal = (msg.value * pricePerWei()) / 1 ether;
    unlb.mint(msg.sender, _tokenVal);
  }


  function finish(address _team, address _fund, address _bounty) external onlyOwner {
    require(now >= ICO_END_DATE && !isFinished);
    unlb.unpause();
    isFinished = true;

    uint _total = unlb.totalSupply() * 100 / (100 - 12 - 10 - 2);
    unlb.mint(_team,   (_total * 12) / 100);
    unlb.mint(_fund,   (_total * 10) / 100);
    unlb.mint(_bounty, (_total *  2) / 100);
  }


  function withdraw() external onlyOwner {
    teamWallet.transfer(this.balance);
  }

  function transferTokenOwnership(address _a) external onlyOwner {
    unlb.transferOwnership(_a);
  }
}
