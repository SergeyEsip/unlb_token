pragma solidity ^0.4.15;

import 'zeppelin-solidity/contracts/token/PausableToken.sol';

contract UNLB is PausableToken {

  string public constant name = "Unolabo";
  string public constant symbol = "UNLB";
  uint256 public constant decimals = 18;

  function UNLB() {
    owner = msg.sender;
  }

  function mint(address _x, uint _v) public onlyOwner {
    balances[_x] += _v;
    totalSupply += _v;
    Transfer(0x0, _x, _v);
  }
}
